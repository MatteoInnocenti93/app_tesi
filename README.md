# Share City App

### Per buildare il backend
Lanciare lo script ***./start.sh*** per buildare e avviare i container che gestiscono il backend.
Una volta finita la build, verrà avviato ngrok.

### Per collegare l'app al server
Copiare l'indirizzo mostrato sul terminale generato da ngrok e incollarlo nella variabile ***INTERNET_ADDRESS*** all'interno della classe ***Utilities*** su Flutter.

### Per accedere al DB
Accedere all'indirizzo ***http://localhost:8094/adminer.php*** una volta avviati i container e inserire i seguenti parametri: 
System: MySQL,
Server: mysql_api_tesi,
User: user, 
Password: password,
Database: api_tesi.

### Per terminare i container del backend
Lanciare lo script ***./stop.sh***
