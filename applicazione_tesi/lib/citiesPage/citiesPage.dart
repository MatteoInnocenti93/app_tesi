import 'package:applicazione_tesi/citiesPage/cityButton.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/confirmationAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/genericComponents/logo.dart';
import 'package:applicazione_tesi/loginPage/loginPage.dart';
import 'package:applicazione_tesi/models/city.dart';
import 'package:applicazione_tesi/myDataPage/myDataPage.dart';
import 'package:applicazione_tesi/services/favCitiesService.dart';
import 'package:applicazione_tesi/services/getCitiesService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:shared_preferences/shared_preferences.dart';

class _CitiesPage extends State<CitiesPage> {

  bool _justFavorites = false;
  final List<City> _cities = [];
  final List<City> _favCities = [];
  final List<City> _citiesToShow = [];
  final ValueNotifier<int> _number = new ValueNotifier(0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ValueListenableBuilder<int>(
            valueListenable: this._number,
            builder: (context, value, child) {
            return Stack(
            alignment: AlignmentDirectional.topCenter,
            children: <Widget>[
          Container(
          decoration: Decorations.imageLogin()),
          Container(
          child: WillPopScope(
              onWillPop: () => this._confirmGetOut(context),
              child: Center(
                  child: SingleChildScrollView(child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Logo(),
                        SizedBox(height: Utilities.verticalRatio * 0.6),
                        Container(
                            height: Utilities.verticalRatio * 26,
                            width: Utilities.FIELDS_WIDTH ,
                            decoration: Decorations.mainGreyDecorationContainer(),
                            child: Column(children: <Widget>[
                              SizedBox(height: Utilities.verticalRatio * 0.8,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(width: Utilities.FIELDS_WIDTH * 0.7, decoration: Decorations.textBoxDecorationContainer(),
                                      child: TextFormField(onChanged: (text)  {
                                        this._filterList(text.toLowerCase());
                                      }, decoration: new InputDecoration(border: InputBorder.none,
                                          contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                                          hintText: 'Cerca città', labelStyle: TextStyle(fontSize: 20)),
                                      )
                                  ),
                                  IconButton(
                                    icon: Icon(this._justFavorites ? Icons.star : Icons.star_border),
                                    onPressed: this._showJustFavoritesCities,
                                  ),
                                ],),
                              SizedBox(height: Utilities.verticalRatio,),
                              HorizontalLine(Colors.grey[600], Utilities.FIELDS_WIDTH),
                              Expanded(
                                child: Align(
                                alignment: Alignment.bottomCenter,
                                  child: Container(
                                    height: Utilities.verticalRatio * 50,
                                    width: Utilities.FIELDS_WIDTH * 2,
                                    child: SingleChildScrollView(
                                      child: FutureBuilder(
                                          future: this._getCities(),
                                          builder: (context, snapshot) => snapshot.hasData ?
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              this._citiesToShow.length > 0 ?
                                              Container(width: Utilities.FIELDS_WIDTH * 0.9, child: ListView.separated(
                                                itemCount: this._citiesToShow.length,
                                                physics: NeverScrollableScrollPhysics(),
                                                shrinkWrap: true, itemBuilder: (context, index) {
                                                  final City city = this._citiesToShow.elementAt(index);
                                                  return CityButton(city, this._isFavorite(city), this._favCities);
                                              },
                                                separatorBuilder: (context, index) => SizedBox(height: Utilities.verticalRatio,),
                                              ),
                                              ) : Text(snapshot.data),
                                              SizedBox(height: Utilities.verticalRatio,)
                                            ],) : snapshot.hasError ? Text("Errore nel caricamento dei dati.") : LinearProgressIndicator()
                                      )
                              ),))),
                  ]
                  )
              ),
                        SizedBox(height: Utilities.verticalRatio,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            SecondaryButton("I miei dati", this._goToMyDataPage),
                            SecondaryButton("Logout", () { this._confirmGetOut(context); }),
                        ],),
          ])
     ))))]);
    }));
  }

  Future <String> _getCities() async {
    if (this._cities.isEmpty) {
      final cities = await GetCitiesService().attemptGetCities();
      this._loadFavCities();
      this._cities.clear();
      this._cities.addAll(cities);
      this._citiesToShow.clear();
      this._citiesToShow.addAll(cities);
    }
    return "";
  }

  void _filterList(final String filter) {

    if (!this._justFavorites) {
      setState(() {
        if (filter == "") {
          this._citiesToShow.clear();
          this._citiesToShow.addAll(this._cities);
        } else {
          this._citiesToShow.clear();
          this._citiesToShow.addAll(this._cities.where((elem) =>
                                    elem.name.toLowerCase().contains(filter)
                                        || elem.country.toString().toLowerCase().contains(filter)
                                        || elem.province.toString().toLowerCase().contains(filter)));
        }
      });
    } else {
      setState(() {
        if (filter == "") {
          this._citiesToShow.clear();
          this._citiesToShow.addAll(this._favCities);
        } else {
          this._citiesToShow.clear();
          this._citiesToShow.addAll(this._favCities.where((elem) =>
                                    elem.name.toLowerCase().contains(filter)
                                        || elem.country.toString().toLowerCase().contains(filter)
                                        || elem.province.toString().toLowerCase().contains(filter)));
        }
      });
    }
  }

  bool _isFavorite(final City city) {
    bool isFavorite = false;
    this._favCities.forEach((elem) {
      if (elem.name == city.name && elem.country == city.country) {
        isFavorite = true;
      }
    });
    return isFavorite;
  }

  _goToMyDataPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyDataPage()));
  }


  _confirmGetOut(final BuildContext context) {
    ConfirmationAlertDialog(this._exit, "Attenzione", "Sei sicuro di voler uscire?", context).show();
  }

  void _exit(final BuildContext context) {
    while (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
    Utilities.logout();
  }

  void _loadFavCities() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.getString('email');
    FavCitiesService().attemptGetFavCities(email).then((result) {
      if (result != null) {
        this._favCities.clear();
        this._favCities.addAll(result);
      }
    });
  }

  _showJustFavoritesCities() async {
    if (this._favCities.isEmpty) {
      this._loadFavCities();
    }
    this.setState(() {
      this._justFavorites = !this._justFavorites;
      if(this._justFavorites) {
        this._citiesToShow.clear();
        this._citiesToShow.addAll(this._favCities);
      } else {
        this._citiesToShow.clear();
        this._citiesToShow.addAll(this._cities);
      }
    });
  }

}

class CitiesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CitiesPage();
}