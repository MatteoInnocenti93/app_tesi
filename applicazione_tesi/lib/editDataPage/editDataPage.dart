import 'dart:ui';
import 'package:applicazione_tesi/genericComponents/buttons/mainButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/customTextField.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/genericComponents/inputValidators.dart';
import 'package:applicazione_tesi/genericComponents/titleText.dart';
import 'package:applicazione_tesi/myDataPage/myDataPage.dart';
import 'package:applicazione_tesi/services/internetService.dart';
import 'package:applicazione_tesi/services/userService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../genericComponents/logoAndBackButton.dart';
import '../utilities.dart';

class _EditDataPage extends State<EditDataPage> {

  static const int _NAME_INDEX = 0;
  static const int _SURNAME_INDEX = 1;
  static const int _OLDPASS_INDEX = 2;
  static const int _PASSWORD_INDEX = 3;
  static const int _CONF_PASSWORD_INDEX = 4;
  final List<TextEditingController> _controllers = [TextEditingController(), TextEditingController(), TextEditingController(),
    TextEditingController(), TextEditingController()];
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
        body: Stack(children: <Widget>[
          Container(decoration: Decorations.imageLogin()),
          SingleChildScrollView(
            child: Form(autovalidate: true, key: this._formKey, child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: Utilities.verticalRatio * 3), //space between the fields
                LogoAndBackButton(),
                SizedBox(
                  height: Utilities.verticalRatio * 1.5,
                ),
                Container(
                    decoration: Decorations.mainGreyDecorationContainer(),
                    width: Utilities.horizontalRatio * 33,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: Utilities.verticalRatio,),
                        TitleText('Modifica', TextAlign.center, true),
                        HorizontalLine(Colors.grey[600], Utilities.FIELDS_WIDTH),
                        FutureBuilder(
                            future: this._getUser(),
                            builder: (context, snapshot) => snapshot.hasData ?
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                CustomTextField(
                                    false,
                                    this._controllers.elementAt(_NAME_INDEX),
                                    TextInputType.text,
                                        (value) { return InputValidators.nameValidator(value); },
                                    "Nome",

                                ),
                                SizedBox(
                                    height:
                                    Utilities.verticalRatio), //space between the fields
                                CustomTextField(
                                    false,
                                    this._controllers.elementAt(_SURNAME_INDEX),
                                    TextInputType.text,
                                        (value) { return InputValidators.surnameValidator(value); },
                                    "Cognome"
                                ), //name text field
                                SizedBox(
                                    height:
                                    Utilities.verticalRatio), //space between the fields
                                CustomTextField(
                                    true,
                                    this._controllers.elementAt(_OLDPASS_INDEX),
                                    TextInputType.text, (value) { return null; },
                                    "Vecchia password"), //name text field
                                SizedBox(
                                    height:
                                    Utilities.verticalRatio), //space between the fields
                                CustomTextField(
                                    true,
                                    this._controllers.elementAt(_PASSWORD_INDEX),
                                    TextInputType.text, (value) { return null; },
                                    "Password"), //name text field
                                SizedBox(
                                    height:
                                    Utilities.verticalRatio), //space between the fields
                                CustomTextField(
                                    true,
                                    this._controllers.elementAt(_CONF_PASSWORD_INDEX),
                                    TextInputType.text, (value) { return null; },
                                    "Conferma password"), //name text field
                                SizedBox(
                                    height:
                                    Utilities.verticalRatio), //space between the fields
                              ],) : snapshot.hasError ? Text("Errore nel caricamento dei dati.") : LinearProgressIndicator()
                        ),
                        MainButton("Modifica", this._editUser, 150),
                        SizedBox(height: Utilities.verticalRatio),
                      ],
                    )),
                SizedBox(height: Utilities.verticalRatio * 2), //space between the fields
              ],
            ),
            ),
          )
        ],));
  }

  void _editUser() {
    InternetService.checkConnectivity().then((isConnected) {
      if (isConnected) {
        if (this._formKey.currentState.validate()) {
          this._checkInput(context);
        }
      } else {
        CustomAlertDialog(context, "Attenzione", "Controllare che lo smartphone sia collegato ad internet.").show();
      }
    });
  }

  _getUser() async {
    UserService().attemptGetUser().then((result) {
      if (result == null) {
        CustomAlertDialog(context, "Attenzione", "Utente non trovato.").show();
      } else {
          this._controllers.elementAt(_NAME_INDEX).text = result.name;
          this._controllers.elementAt(_SURNAME_INDEX).text = result.surname;
      }
    });
    return "";
  }

  void _checkInput(final BuildContext context) {

      final name = this._controllers.elementAt(_NAME_INDEX).text;
      final surname = this._controllers.elementAt(_SURNAME_INDEX).text;
      final oldPassword = this._controllers.elementAt(_OLDPASS_INDEX).text;
      final password = this._controllers.elementAt(_PASSWORD_INDEX).text;
      final confirmPassword = this._controllers.elementAt(_CONF_PASSWORD_INDEX).text;

      if (name != "" && surname != "") {
        if (oldPassword == "" && password == "" && confirmPassword == "") {
          UserService().attemptUpdateUser(name, surname, '', '').then((result) {
            if (result.isEmpty) {
              this._showSuccessDialog();
            }
          });
        } else {
          if (oldPassword != "" && password != "" && confirmPassword != "") {
            final result = InputValidators.passwordValidator(password);
            if (password == confirmPassword && result == null) {
              UserService().attemptUpdateUser(
                  name, surname, oldPassword, password).then((result) {
                if (result.isEmpty) {
                  this._showSuccessDialog();
                } else {
                  CustomAlertDialog(
                      context, "Errore", "La password attuale è errata.")
                      .show();
                }
              });
            } else {
              CustomAlertDialog(
                  context, "Errore", "Le password devono corrispondere.\n" + result)
                  .show();
            }
          } else {
            CustomAlertDialog(context, "Errore", "Per cambiare la password inserire quella vecchia.\n"
                                                  "Le due password devono coincidere.").show();
            this._disableKeyboard(context);
          }
        }
      } else {
        CustomAlertDialog(context, "Errore", "Riempire TUTTI i campi e controllare che siano corretti.").show();
        this._disableKeyboard(context);
      }
  }

  void _showSuccessDialog() {
    showDialog(
        context: context,
        builder: (final BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15)),
            title: Text(
              "Profilo modificato",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
            ),
            content: Text(
              "Modifica dell'account avvenuta con successo!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 19,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text(
                    'OK',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                MyDataPage()
                        )
                    );
                  }),
            ],
          );
        });
  }

  void _disableKeyboard(context) {
    final FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

}

class EditDataPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _EditDataPage();
}
