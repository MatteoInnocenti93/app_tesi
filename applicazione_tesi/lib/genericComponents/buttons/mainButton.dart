import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utilities.dart';
import '../decorations.dart';

class MainButton extends StatelessWidget {

  final String _text;
  final Function _function;
  final double _width;

  MainButton(this._text, this._function, this._width);

  @override
  Widget build(final BuildContext context) {
    return Container(
        decoration: Decorations.buttonBorders(),
        height: Utilities.verticalRatio * 2.1,
        width: this._width,
        child: RaisedButton(
          onPressed: this._function,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                  Utilities.BUTTON_RADIUS)),
          padding: EdgeInsets.all(0.0),
          child: Decorations.gradientButton(this._text),
        ));
  }

}