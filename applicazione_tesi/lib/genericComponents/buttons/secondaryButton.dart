import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utilities.dart';
import '../decorations.dart';

class SecondaryButton extends StatelessWidget {

  final String _text;
  final Function _function;

  SecondaryButton(this._text, this._function);

  @override
  Widget build(final BuildContext context) {
    return Container(
        height: Utilities.FIELDS_HEIGHT,
        decoration: Decorations.loginButtons(),
        child: FlatButton(
            child: Text(
                this._text,
                style: TextStyle(
                    color: Colors.blueGrey[700],
                    fontSize: 16,
                    fontWeight: FontWeight.bold)
            ), onPressed: this._function));
  }

}