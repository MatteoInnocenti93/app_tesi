import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utilities.dart';

class ConfirmationAlertDialog {

  final Function(BuildContext) _function;
  final BuildContext _context;
  final String _title;
  final String _text;

  ConfirmationAlertDialog(this._function, this._title, this._text, this._context);

  void show() {
    showDialog(context: this._context,
        builder: (final BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Utilities.BUTTON_RADIUS)),
            title: Text(
              this._title,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            content: Text(
              this._text,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 17),
            ),
            actions: [
              FlatButton(
                child: Text(
                  "No",
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                ),
                onPressed: () { Navigator.of(context).pop(); },
              ),
              FlatButton(
                  child: Text(
                    "Si",
                    style: TextStyle(fontSize: 17),
                  ),
                  onPressed: () { this._function(context); }
              ),
            ],
          );
        });
  }

}