import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utilities.dart';

class CustomTextField extends StatelessWidget {

  final bool _isAPassword;
  final TextEditingController _textController;
  final TextInputType _inputType;
  final Function _validator;
  final String _text;

  CustomTextField(this._isAPassword, this._textController, this._inputType, this._validator, this._text);

  @override
  Widget build(final BuildContext context) {
    return Container(
      width: Utilities.FIELDS_WIDTH - Utilities.horizontalRatio * 4,
      child: TextFormField(cursorColor: Colors.black,
        keyboardType: this._inputType,
        obscureText: this._isAPassword,
        controller: this._textController,
        validator: this._validator,
        decoration: new InputDecoration(
            errorMaxLines: 2,
            contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
            labelText: _text, labelStyle: TextStyle(fontSize: 20)),
      ),
    ); //name text field
  }
}
