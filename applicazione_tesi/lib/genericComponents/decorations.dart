import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utilities.dart';

class Decorations {

  static BoxDecoration textBoxDecorationContainer() {
    return BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              blurRadius: 5,
              spreadRadius: 0.5,
              offset: Offset(3.0, 3.0)),
        ],
        borderRadius: BorderRadius.circular(Utilities.BUTTON_RADIUS),
        color: Colors.white);
  }

  static BoxDecoration containerLogin() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(Utilities.BUTTON_RADIUS),
        color: Colors.grey[200]);
  }

  static BoxDecoration message() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(Utilities.BUTTON_RADIUS),
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Colors.blue[300], Colors.lightBlue[100]]),);
  }

  static BoxDecoration messageOwner() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(Utilities.BUTTON_RADIUS),
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Colors.orange[100], Colors.yellow[50]]),);
  }

  static BoxDecoration mainGreyDecorationContainer() {
    return BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Colors.grey[100], Colors.blueGrey[100]]),
      boxShadow: [
        BoxShadow(
            color: Colors.grey[600],
            blurRadius: 2,
            spreadRadius: 1.0,
            offset: Offset(3.0, 3.0)),
      ],
      borderRadius: BorderRadius.all(Radius.circular(20)),
    );
  }

  static BoxDecoration itemDecorationContainer() {
    return BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Color(0xff26273b), Colors.blueGrey[100]]),

      borderRadius: BorderRadius.all(Radius.circular(30)),
    );
  }

  static BoxDecoration buttonBorders() {
    return BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey[400],
              blurRadius: 10,
              spreadRadius: 1.0,
              offset: Offset(3.0, 3.0)),
        ],
        borderRadius: BorderRadius.circular(
            Utilities.BUTTON_RADIUS),
        color: Colors.grey[400]);
  }

  static BoxDecoration imageLogin() {
    return BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/vert.jpg'), fit: BoxFit.cover));
  }

  static BoxDecoration imageHomepage() {
    return BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/home.jpg'), fit: BoxFit.cover));
  }

  static BoxDecoration loginButtons() {
    return BoxDecoration(

        borderRadius: BorderRadius.circular(
            Utilities.BUTTON_RADIUS),
        color: Colors.blueGrey[100]);
  }

  static Ink gradientButton(final String text) {
    return Ink(
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.indigo[600],
              Colors.lightBlue[200]
            ],
          ),
          borderRadius: BorderRadius.circular(
              Utilities.BUTTON_RADIUS)),
      child: Container(
        constraints: BoxConstraints(
            maxWidth: 200.0, minHeight: 1.0),
        alignment: Alignment.center,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
    );
  }

}