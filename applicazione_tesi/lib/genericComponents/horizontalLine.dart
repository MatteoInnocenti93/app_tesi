import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class HorizontalLine extends StatelessWidget {

  final Color _color;
  final double _width;

  HorizontalLine(this._color, this._width);

  @override
  Widget build(final BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 1.0), //horizontal line
      child: Container(
        height: 1.0,
        width: this._width,
        color: this._color,
      ),
    );
  }

}