import 'package:email_validator/email_validator.dart';

class InputValidators {

  static String nameValidator(final String value) {
    if (value.length > 30) {
      return "Lunghezza max. 30 caratteri.";
    } else if (value.isEmpty) {
      return "Inserire un nome.";
    }
    return null;
  }

  static String surnameValidator(final String value) {
    if (value.length > 30) {
      return "Lunghezza max. 30 caratteri.";
    } else if (value.isEmpty) {
      return "Inserire un cognome.";
    }
    return null;
  }

  static String emailValidator(final String value) {
    if (!EmailValidator.validate(value)) {
      return "Inserire una mail valida.";
    }
    return null;
  }

  static String passwordValidator(final String value) {
    final String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$';
    final RegExp regExp = new RegExp(pattern);
    if(regExp.hasMatch(value)) {
      return null;
    }
    return "Inserire almeno una lettera maiuscola, minuscola e un numero (lunghezza min. 6)";
  }
}