import 'package:applicazione_tesi/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class LogoAndBackButton extends StatelessWidget {

  @override
  Widget build(final BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      IconButton(
        icon: Icon(Icons.arrow_back_ios),
        onPressed: () { Navigator.pop(context); },
        color: Colors.grey[300],
      ),
      Flexible(
        child: Image.asset(
          'assets/images/logo.png',
          width: 300,//main logo
          height: 70,
        ),
      ),
      SizedBox(
        width: Utilities.verticalRatio,
      )
    ]);
  }

}