import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/roomPage/roomPage.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class RoomButton extends StatelessWidget {

  static const double _CONTAINER_HEIGHT = 120;

  final Room _room;

  RoomButton(this._room);

  @override
  Widget build(final BuildContext context) {
    return this._getRoomButton(context);
  }

  Widget _getRoomButton(final BuildContext context) {
    return Container(
        decoration: Decorations.itemDecorationContainer(),
        height: _CONTAINER_HEIGHT * 0.8, // height of the button
        width: Utilities.FIELDS_WIDTH * 0.9,
        child: FlatButton(onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => RoomPage(this._room)));
        },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "La stanza \ndi " + this._room.name ,
                        style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white),),
                    ],),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(height: _CONTAINER_HEIGHT * 0.6,
                          child: Image.asset('assets/images/cityLogo2.png')),
                    ],)
                ])
        ));
  }
}