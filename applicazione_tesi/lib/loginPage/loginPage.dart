import 'package:applicazione_tesi/citiesPage/citiesPage.dart';
import 'package:applicazione_tesi/genericComponents/buttons/mainButton.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/customTextField.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/registerPage/registerPage.dart';
import 'package:applicazione_tesi/services/internetService.dart';
import 'package:applicazione_tesi/services/loginService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../utilities.dart';


class _LoginPageState extends State<LoginPage> {

  final _mailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
        body: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: <Widget>[
            Container(
                decoration: Decorations.imageHomepage()),
            SingleChildScrollView(
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: Utilities.verticalRatio * 8), //space between the fields
                      Image.asset('assets/images/logo.png', width: Utilities.FIELDS_WIDTH), //main logo
                      SizedBox(height: Utilities.verticalRatio), //space between the fields
                      Container(
                          decoration: Decorations.mainGreyDecorationContainer(),
                          width: Utilities.horizontalRatio * 33,
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: Utilities.verticalRatio * 0.5,
                              ),
                              CustomTextField(
                                  false,
                                  this._mailController,
                                  TextInputType.emailAddress,
                                      (value) { return null; },
                                  "E-mail"
                              ), //e-mail text field
                              SizedBox(
                                  height:
                                  Utilities.verticalRatio), //space between the fields
                              CustomTextField(
                                  true,
                                  this._passwordController,
                                  TextInputType.text,
                                      (value) { return null; },
                                  "Password"
                              ), //password text-field
                              SizedBox(
                                  height: Utilities.verticalRatio *
                                      1.5), //space between the fields
                              MainButton("Accedi", this._functionOnLoginButton, 150),
                              SizedBox(
                                  height: Utilities.verticalRatio),
                            ],
                          )),
                      SizedBox(
                          height: Utilities.verticalRatio), //space between the fields
                      SecondaryButton("Nuovo account", () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage()));
                      }),
                      SizedBox(
                          height: Utilities.verticalRatio),
                    ],
                  ),
                )
            )
          ],)
    );
  }

  void _functionOnLoginButton() {
    InternetService.checkConnectivity().then((isConnected) {
      if (isConnected) {
        if (!this._checkEmptyFields()) {
          this._getJWT(this._mailController.text, this._passwordController.text).then((jwt) {
            if (jwt != null) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          CitiesPage()
                  )
              );
            } else {
              CustomAlertDialog(context, "Errore",
                  "E-mail o password non corretti.")
                  .show();
            }
          });
        } else {
          CustomAlertDialog(context, "Errore",
              "E-mail o password non inseriti.").show();
        }
      } else {
        CustomAlertDialog(context, "Attenzione", "Controllare che lo smartphone sia collegato ad internet.").show();
      }
    });
  }

  Future<dynamic> _getJWT(final String email, final String password) async {
    final jwt = await LoginService().attemptLogin(email, password);
    return jwt;
  }



  bool _checkEmptyFields() {
    return this._mailController.text == "" || this._passwordController.text == "";
  }

}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}
