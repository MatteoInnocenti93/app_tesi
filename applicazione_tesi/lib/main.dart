import 'package:applicazione_tesi/citiesPage/citiesPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:panorama/panorama.dart';

import 'loginPage/loginPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: this._userIsLoggedIn() ? CitiesPage() : LoginPage(),
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        fontFamily: 'Montserrat'
      ),
    );
  }

  bool _userIsLoggedIn() {
    final FlutterSecureStorage storage = FlutterSecureStorage();
    print(storage.read(key: 'jwt') != null);
    return storage.read(key: 'jwt') != null;
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  static final double _iconSize = 60;
  static final double _spaceSize = 10;
  double latitude = 0;
  double longitude = 0;


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: GestureDetector(
          onTapDown: _handleTapDown ,
          child: Stack(children: <Widget>[
            Panorama(animSpeed: 1,child: Image.asset('images/room.jpg')),
            Column(children: <Widget>[
              SizedBox(height: _spaceSize,),
              FlatButton(onPressed: () {},child: Container(width: _iconSize, height: _iconSize, child: Image.asset('images/1.jpg'))),
              SizedBox(height: _spaceSize,),
              FlatButton(onPressed: () {},child: Container(width: _iconSize, height: _iconSize, child: Image.asset('images/2.png'))),
              SizedBox(height: _spaceSize,),
              FlatButton(onPressed: () {},child: Container(width: _iconSize, height: _iconSize, child: Image.asset('images/3.png'))),
              SizedBox(height: _spaceSize,),
              FlatButton(onPressed: () {},child: Container(width: _iconSize, height: _iconSize, child: Image.asset('images/4.jpg'))),
              SizedBox(height: _spaceSize,),
              FlatButton(onPressed: () {},child: Container(width: _iconSize, height: _iconSize, child: Image.asset('images/5.png'))),
            ],)
          ],),
        ));
  }

  Offset _tapPosition;

  void _handleTapDown(TapDownDetails details) {
    final RenderBox referenceBox = context.findRenderObject();
    _tapPosition = referenceBox.globalToLocal(details.globalPosition);
    print(_tapPosition.dx);
    print(_tapPosition.dy);
  }

}
