
class City {

  String _cityID;
  String _name;
  String _country;
  String _province;
  double _longitude;
  double _latitude;
  double _distance;

  City(this._cityID, this._name, this._country, this._province, this._latitude, this._longitude, this._distance);

  String get cityID => _cityID;

  String get name => _name;

  String get country => _country;

  String get province => _province;

  double get longitude => _longitude;

  double get latitude => _latitude;
  
  double get distance => _distance;

  set distance(double value) {
    _distance = value;
  }

  factory City.fromJson(final dynamic json, final double distance) {
    return City(json['id'] as String, json['name'] as String, json['country'] as String, json['province'] as String,
                double.parse(json['latitude']), double.parse(json['longitude']), distance);
  }
}