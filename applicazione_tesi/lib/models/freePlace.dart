
class FreePlace {

  String _cityID;
  int _startX;
  int _startY;
  int _endX;
  int _endY;
  int _offset;

  FreePlace(this._cityID, this._startX, this._startY, this._endX, this._endY, this._offset);

  String get cityID => _cityID;

  int get startX => _startX;

  int get startY => _startY;

  int get endY => _endY;

  int get endX => _endX;

  int get offset => _offset;

  factory FreePlace.fromJson(final dynamic json) {
    return FreePlace(json['city_id'] as String, int.parse(json['start_x']), int.parse(json['start_y']),
                    int.parse(json['end_x']), int.parse(json['end_y']), int.parse(json['offset']));
  }
}