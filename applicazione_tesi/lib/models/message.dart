
class Message {

  String _messageID;
  String _nameSender;
  String _emailSender;
  String _text;
  String _roomID;
  String _time;

  Message(this._messageID, this._nameSender, this._emailSender, this._text, this._roomID, this._time);

  String get messageID => _messageID;

  String get nameSender => _nameSender;

  String get emailSender => _emailSender;

  String get text => _text;

  String get roomID => _roomID;

  String get time => _time;

  factory Message.fromJson(final dynamic json) {
    return Message(json['id'] as String, json['name_sender'] as String,
                   json['email_sender'] as String, json['text'] as String,
                   json['room_id'] as String, json['created_at'] as String);
  }
}