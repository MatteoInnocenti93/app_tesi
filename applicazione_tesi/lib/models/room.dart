class Room {

  String _roomID;
  String _emailUser;
  String _nameUser;
  String _surnameUser;
  String _contacts;
  String _work;
  String _interests;
  String _tour;
  String _latitude;
  String _longitude;
  String _city;
  String _country;
  String _imagePath;

  Room(this._emailUser, this._nameUser, this._surnameUser, this._contacts, this._work, this._interests, this._tour,
      this._latitude, this._longitude, this._city, this._country, this._roomID, this._imagePath);

  String get roomID => _roomID;

  String get emailUser => _emailUser;

  String get name => _nameUser;

  String get surname => _surnameUser;

  String get contacts => _contacts;

  String get work => _work;

  String get interests => _interests;

  String get tour => _tour;

  String get latitude => _latitude;

  String get longitude => _longitude;

  String get city => _city;

  String get country => _country;

  String get imagePath => _imagePath;

  factory Room.fromJson(final dynamic json) {
    return Room(json['email_user'] as String, json['name'] as String, json['surname'] as String,
                json['contacts'] as String, json['work'] as String,
                json['interests'] as String, json['tour'] as String, json['latitude'] as String,
                json['longitude'] as String, json['city'] as String, json['country'] as String, json['id'] as String,
                json['image_path'] as String);
  }
}