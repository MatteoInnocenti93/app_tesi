
class User {

  String _name;
  String _surname;
  String _email;

  User(this._name, this._surname, this._email);

  String get name => _name;

  String get surname => _surname;

  String get email => _email;

  factory User.fromJson(final dynamic json) {
    return User(json['name'] as String, json['surname'] as String, json['email'] as String);
  }
}