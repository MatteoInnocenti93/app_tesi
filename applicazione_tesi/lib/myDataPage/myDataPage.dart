import 'package:applicazione_tesi/editDataPage/editDataPage.dart';
import 'package:applicazione_tesi/editRoomPage/editRoomPage.dart';
import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/genericComponents/normalText.dart';
import 'package:applicazione_tesi/genericComponents/titleText.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/newRoomPage/newRoomPage.dart';
import 'package:applicazione_tesi/roomPage/roomPage.dart';
import 'package:applicazione_tesi/services/roomsService.dart';
import 'package:applicazione_tesi/services/userService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../genericComponents/logoAndBackButton.dart';
import '../utilities.dart';

class _MyDataPage extends State<MyDataPage> {

  Room _myRoom;
  String _email = "";
  String _name = "";
  String _surname = "";

  _MyDataPage() {
    this._loadData();
  }

  @override
  Widget build(final BuildContext context) {

    return Scaffold(
        body: Stack(children: <Widget>[
          Container(decoration: Decorations.imageLogin()),
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                LogoAndBackButton(),
                SizedBox(height: Utilities.verticalRatio,),
                Container(
                    height: Utilities.verticalRatio * 21,
                    width: Utilities.FIELDS_WIDTH ,
                    decoration: Decorations.mainGreyDecorationContainer(),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: Utilities.verticalRatio,),
                          TitleText('I miei dati', TextAlign.left, true),
                          HorizontalLine(Colors.grey[600], Utilities.FIELDS_WIDTH),
                          SizedBox(height: Utilities.verticalRatio * 2,),
                          NormalText("E-mail: " + this._email, true),
                          SizedBox(height: Utilities.verticalRatio,),
                          NormalText("Nome: " + this._name, true),
                          SizedBox(height: Utilities.verticalRatio,),
                          NormalText("Cognome: " + this._surname, true),
                          SizedBox(height: Utilities.verticalRatio * 2,),
                          SecondaryButton('Modifica dati', () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => EditDataPage()));
                          }),
                          SizedBox(height: Utilities.verticalRatio,),
                          SecondaryButton(this._myRoom != null ? 'Vedi la stanza' : 'Crea la stanza', () {
                            if (this._myRoom != null) {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => RoomPage(this._myRoom)));
                            } else {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => NewRoomPage()));
                            }
                          }),
                          SizedBox(height: Utilities.verticalRatio,),
                          this._myRoom != null ? SecondaryButton('Modifica stanza', () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => EditRoomPage(this._myRoom)));
                          }) : Text(''),
                      ],),
                )
            ],),
          )
        ],));
  }

  _loadData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final Room myRoom = await RoomsService().attemptGetMyRoom(prefs.getString('email'));
    final user = await UserService().attemptGetUser();
    if (user == null) {
      CustomAlertDialog(context, "Attenzione", "Utente non trovato.").show();
    } else {
      this._name = user.name;
      this._surname = user.surname;
    }
    this._email = prefs.getString('email');
    this.setState(() {
      if (myRoom != null) {
        this._myRoom = myRoom;
      }
    });
  }

}

class MyDataPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyDataPage();
}
