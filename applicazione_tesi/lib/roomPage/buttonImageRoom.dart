import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class ButtonImageRoom extends StatelessWidget {

  final _image;
  final _iconSize;

  ButtonImageRoom(this._image, this._iconSize);

  @override
  Widget build(final BuildContext context) {
    return Container(width: _iconSize * 1.2, height: _iconSize * 1.2, child: Image.asset(this._image));
  }

}