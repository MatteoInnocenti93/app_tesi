import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/models/review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utilities.dart';

class ReviewContainer extends StatelessWidget {

  final double _width;
  final Review _review;
  final bool _isMe;

  ReviewContainer(this._width, this._review, this._isMe);

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
        child:Column(
          children: <Widget>[
            Container(
              decoration: this._isMe ? Decorations.messageOwner() :  Decorations.containerLogin(),
              child: Padding(
                  padding: EdgeInsets.fromLTRB(13, 10, 13, 10),
                  child: Column(children: <Widget>[
                    Container(
                      width: this._width,
                      child:
                      Row(children: <Widget>[
                        Icon(Icons.star, color: Colors.yellow[800],),
                        Text(this._review.value.toString()),
                        Container(
                          width: this._width * 0.8,
                          child: Text(
                            this._review.nameSender,
                            style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.right, // has impact
                          ),
                        ),
                      ],),
                    ),
                    Container(
                      width: this._width * 0.9,
                      child: Text(
                        this._review.text,
                        style: TextStyle(fontSize: 17),
                        textAlign: TextAlign.right, // has impact
                      ),
                    ),
                  ])),
            ),
            SizedBox(height: Utilities.verticalRatio * 0.7,),
          ],
        ));
  }
}
