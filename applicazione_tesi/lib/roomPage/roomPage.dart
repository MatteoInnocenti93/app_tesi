import 'package:applicazione_tesi/genericComponents/buttons/secondaryButton.dart';
import 'package:applicazione_tesi/genericComponents/customAlertDialog.dart';
import 'package:applicazione_tesi/genericComponents/decorations.dart';
import 'package:applicazione_tesi/genericComponents/horizontalLine.dart';
import 'package:applicazione_tesi/genericComponents/normalText.dart';
import 'package:applicazione_tesi/genericComponents/titleText.dart';
import 'package:applicazione_tesi/models/message.dart';
import 'package:applicazione_tesi/models/review.dart';
import 'package:applicazione_tesi/models/room.dart';
import 'package:applicazione_tesi/roomPage/buttonImageRoom.dart';
import 'package:applicazione_tesi/roomPage/reviewContainer.dart';
import 'package:applicazione_tesi/roomPage/starReview.dart';
import 'package:applicazione_tesi/services/messagesService.dart';
import 'package:applicazione_tesi/services/reviewService.dart';
import 'package:applicazione_tesi/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:panorama/panorama.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'messageContainer.dart';

class _RoomPage extends State<RoomPage> {

  static final double _iconSize = 60;
  static final double _spaceSize = 10;

  final Room _room;
  final TextEditingController _messageController = TextEditingController();
  final TextEditingController _reviewController = TextEditingController();
  final ScrollController _scrollControllerMessages = ScrollController();
  int _itemSelected = 0;
  double _infoWidth = 0;
  double _infoHeight = 0;
  String _title = "";
  String _text = "";
  String _myMail = "";
  List<Message> _messages = [];
  List<Review> _reviews = [];
  int _indexReviewDone = -1;

  _RoomPage(this._room);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          child: Stack(children: <Widget>[
            Panorama(animSpeed: 1.5, child: Image.asset(this._room.imagePath)),
            Padding(padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Column(children: <Widget>[
                  SizedBox(height: _spaceSize,),
                  FlatButton(onPressed: () { this._showInfo(1); },child: ButtonImageRoom('assets/images/contacts.png', _iconSize)),
                  SizedBox(height: _spaceSize,),
                  FlatButton(onPressed: () { this._showInfo(2); },child: ButtonImageRoom('assets/images/work.png', _iconSize)),
                  SizedBox(height: _spaceSize,),
                  FlatButton(onPressed: () { this._showInfo(3); },child: ButtonImageRoom('assets/images/interests.png', _iconSize)),
                  SizedBox(height: _spaceSize,),
                  FlatButton(onPressed: () { this._showInfo(4); },child: ButtonImageRoom('assets/images/tour.png', _iconSize)),
                  SizedBox(height: _spaceSize,),
                  FlatButton(onPressed: () { this._showInfo(5); },child: ButtonImageRoom('assets/images/message.png', _iconSize)),
                  SizedBox(height: _spaceSize,),
                  FlatButton(onPressed: () { this._showInfo(6); },child: ButtonImageRoom('assets/images/star.png', _iconSize)),
                ],),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 30, 0),
                    child:
                    Align(
                    alignment: Alignment.topCenter,
                    child:
                      AnimatedContainer(
                        curve: Curves.easeInOutQuart,
                        duration: Duration(milliseconds: 500),
                        height: this._infoHeight,
                        width: this._infoWidth,
                        decoration: Decorations.mainGreyDecorationContainer(),
                       child: this._itemSelected == 5 ?
                          Column(children: <Widget>[
                            SizedBox(height: Utilities.verticalRatio,),
                            TitleText(this._title, TextAlign.center, true),
                            HorizontalLine(Colors.grey[800], this._infoWidth * 0.85),
                            Expanded(
                                child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      child: SingleChildScrollView(
                                        controller: this._scrollControllerMessages,
                                          child: FutureBuilder(
                                              future: this._getMessages(),
                                              builder: (context, snapshot) => snapshot.hasData ?
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  this._messages.length > 0 ?
                                                  Container(width: Utilities.FIELDS_WIDTH * 0.9, child: ListView.separated(
                                                    itemCount: this._messages.length,
                                                    physics: NeverScrollableScrollPhysics(),
                                                    shrinkWrap: true,
                                                    itemBuilder: (context, index) {
                                                      final message = this._messages.elementAt(index);
                                                      final isMe = this._myMail == message.emailSender;
                                                      final isOwner = this._room.emailUser == message.emailSender;
                                                      return MessageContainer(this._infoWidth * 0.8, message, isMe, isOwner);
                                                    },
                                                    separatorBuilder: (context, index) => SizedBox(height: 0,),
                                                  ),
                                                  ) :
                                                  Text(''),
                                                ],) : snapshot.hasError ? Text("Errore nel caricamento dei dati.") : LinearProgressIndicator()
                                          )
                                      ),))),
                            Container(width: this._infoWidth * 0.9, decoration: Decorations.textBoxDecorationContainer(),
                                child: Row(children: <Widget>[
                                  SizedBox(width: Utilities.horizontalRatio,),
                                  Expanded(child: TextField(
                                    controller: this._messageController,
                                    decoration: InputDecoration.collapsed(
                                      hintText: "Scrivi.."
                                    ),
                                  )),
                                  IconButton(
                                    icon: Icon(Icons.send),
                                    iconSize: 25,
                                    color: Colors.lightBlue[700],
                                    onPressed: () {
                                      this._sendNewMessage();
                                    },
                                  )
                                ],)
                            ),
                            SizedBox(height: Utilities.verticalRatio,),
                          ],)

                           : this._itemSelected == 6 ?

                       Column(children: <Widget>[
                         SizedBox(height: Utilities.verticalRatio,),
                         TitleText(this._title, TextAlign.center, true),
                         HorizontalLine(Colors.grey[800], this._infoWidth * 0.85),
                         Expanded(
                             child: Align(
                                 alignment: Alignment.bottomCenter,
                                 child: Container(
                                   child: SingleChildScrollView(
                                       child: FutureBuilder(
                                           future: this._getReviews(),
                                           builder: (context, snapshot) => snapshot.hasData ?
                                           Column(
                                             mainAxisAlignment: MainAxisAlignment.center,
                                             mainAxisSize: MainAxisSize.max,
                                             children: <Widget>[
                                               this._reviews.length > 0 ?
                                               Container(width: Utilities.FIELDS_WIDTH * 0.9, child: ListView.separated(
                                                 itemCount: this._reviews.length,
                                                 physics: NeverScrollableScrollPhysics(),
                                                 shrinkWrap: true,
                                                 itemBuilder: (context, index) {
                                                   final review = this._reviews.elementAt(index);
                                                   final isMe = this._myMail == review.emailSender;
                                                   return ReviewContainer(this._infoWidth * 0.8, review, isMe);
                                                 },
                                                 separatorBuilder: (context, index) => SizedBox(height: 0,),
                                               ),
                                               ) :
                                               Text(''),
                                             ],) : snapshot.hasError ? Text("Errore nel caricamento dei dati.") : LinearProgressIndicator()
                                       )
                                   ),))),
                         Container(width: this._infoWidth * 0.94, height: Utilities.verticalRatio * 5, decoration: Decorations.textBoxDecorationContainer(),
                             child: Column(children: <Widget>[
                               StarReview(this._indexReviewDone > -1 ? this._reviews.elementAt(this._indexReviewDone) : null),
                               Row(children: <Widget>[
                                 SizedBox(width: Utilities.horizontalRatio,),
                                 Expanded(child: TextField(
                                   controller: this._reviewController,
                                   decoration: InputDecoration.collapsed(
                                       hintText: this._indexReviewDone != -1 ? "Modifica recensione..." : "Scrivi.."
                                   ),
                                 )),
                                 IconButton(
                                   icon: Icon(Icons.send),
                                   iconSize: 25,
                                   color: Colors.lightBlue[700],
                                   onPressed: () {
                                    this._sendReview();
                                   },
                                 )
                               ],),
                             ],)
                         ) ,
                         SizedBox(height: Utilities.verticalRatio,),
                       ],)
                           :
                          Column(children: <Widget>[
                            SizedBox(height: Utilities.verticalRatio,),
                            TitleText(this._title, TextAlign.center, true),
                            HorizontalLine(Colors.grey[800], this._infoWidth * 0.85),
                            SizedBox(height: Utilities.verticalRatio,),
                            NormalText(this._text, true),
                          ])
                         ),
                      ))
              ],)
            ),
            MediaQuery.of(context).viewInsets.bottom == 0 ?
            Align(
                alignment: Alignment.bottomLeft,
                child:
              Padding(
                padding: EdgeInsets.all(20),
                  child: SecondaryButton('Indietro', () {
                    Navigator.pop(context);
                  }),
            )) : Text(''),
          ],),
        ));
  }

  void _sendNewMessage() {
    if (this._messageController.text != '') {
      if (this._messageController.text.length < 1000) {
        MessagesService()
            .attemptNewMessage(this._room, this._messageController.text)
            .then((result) {
          if (result.isEmpty) {
            this._disableKeyboard(context);
            this._messageController.text = "";
            this.setState(() {
              this._scrollUntilTheEnd();
            });
          } else {
            CustomAlertDialog(context, "Errore", result).show();
          }
        });
      } else {
        CustomAlertDialog(context, "Messaggio troppo lungo", "Massimo 1000 caratteri.").show();
      }
    }
  }

  void _sendReview() async {

    final text = this._reviewController.text;

    if (text != '') {
      if (text.length < 1000) {
        ReviewService().attemptNewReview(this._room, text).then((result) {
          if (result.isNotEmpty) {
            CustomAlertDialog(context, "Errore", result).show();
          } else {
            this._disableKeyboard(context);
            this._reviewController.text = "";
            this.setState(() {
              this._scrollUntilTheEnd();
            });
          }
        });
      } else {
        CustomAlertDialog(context, "Recensione troppo lunga", "Massimo 1000 caratteri.").show();
      }
    }
  }

  void _scrollUntilTheEnd() {
      this._scrollControllerMessages.animateTo(
        this._scrollControllerMessages.position.maxScrollExtent,
        curve: Curves.easeInOutQuint,
        duration: const Duration(milliseconds: 500),
      );
  }

  void _showInfo(final int selection) {
    if (selection == this._itemSelected) {
      this._itemSelected = 0;
      this.setState(() {
        this._infoHeight = 0;
        this._infoWidth = 0;
      });
    } else if (selection != this._itemSelected) {
      this._itemSelected = selection;
      this.setState(() {
        this._infoHeight = 330;
        this._infoWidth = 250;
        this._updateTitles();
        if (this._room != null) {
          this._updateText();
        }
      });
    }
  }

  void _updateTitles() {
    switch(this._itemSelected) {
      case 1:
        this._title = "Contatti";
        break;
      case 2:
        this._title = "Lavoro";
        break;
      case 3:
        this._title = "Interessi";
        break;
      case 4:
        this._title = "Tour guidato";
        break;
      case 5:
        this._title = "Messaggi";
        break;
      case 6:
        this._title = "Recensioni";
        break;
    }
  }

  void _updateText() {
    switch(this._itemSelected) {
      case 1:
        this._text = this._room.contacts;
      break;
      case 2:
        this._text = this._room.work;
      break;
      case 3:
        this._text = this._room.interests;
      break;
      case 4:
        this._text = this._room.tour == "1" ?
                    'L\'utente offre la possibilità di fare tour guidati, contattalo per saperne di più!' :
                    'L\'utente non effettua tour guidati.';
      break;
      case 5:
        this._text = "";
        this._infoHeight = this._infoHeight * 1.4;
      break;
      case 6:
        this._text = "";
        this._infoHeight = this._infoHeight * 1.4;
        this._infoWidth = this._infoWidth * 1.03;
        break;
    }
  }

  void _disableKeyboard(context) {
    final FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  Future<String> _getMessages() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    this._myMail = prefs.getString('email');
    final messages = await MessagesService().attemptGetMessages(this._room.roomID);
    if (messages != null) {
      this._messages.clear();
      this._messages.addAll(messages);
    }
    SchedulerBinding.instance.addPostFrameCallback((_) {
      this._scrollUntilTheEnd();
    });
    return "";
  }

  Future<String> _getReviews () async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    this._myMail = prefs.getString('email');
    final reviews = await ReviewService().attemptGetReviews(this._room.roomID);
    if (reviews != null) {
      this._reviews.clear();
      this._reviews.addAll(reviews);
      final myReview = reviews.where((elem) => elem.emailSender == this._myMail);
      this.setState(() {
        this._title = 'Recensioni - ' + this._getReviewsAverage();
        if (myReview.length > 0) {
          this._indexReviewDone = this._reviews.indexOf(myReview.first);
        }
      });
    }
    SchedulerBinding.instance.addPostFrameCallback((_) {
      this._scrollUntilTheEnd();
    });

    return "";
  }

  String _getReviewsAverage() {
    return (this._reviews.map((review) => review.value).reduce((a, b) => a + b) / this._reviews.length).toStringAsFixed(1);
  }
}

class RoomPage extends StatefulWidget {

  final Room _room;

  RoomPage(this._room);

  @override
  State<StatefulWidget> createState() => _RoomPage(this._room);
}
