import 'package:applicazione_tesi/models/review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class _StarReview extends State<StarReview> {

  final double _iconSize = 27;
  final Color _iconsColor = Colors.yellow[800];
  final Icon _emptyStar = Icon(Icons.star_border);
  final Icon _fullStar = Icon(Icons.star);
  final List<bool> _selected = [ false, false, false, false, false ];
  Review _reviewDone;
  bool _reviewLoaded = false;

  _StarReview(this._reviewDone) {
    if (this._reviewDone != null && !this._reviewLoaded) {
      this._reviewLoaded = true;
      for (int i = 0; i < this._reviewDone.value; i++) {
        this._selected[i] = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
          icon: this._showStar(0),
          iconSize: this._iconSize,
          color: this._iconsColor,
          onPressed: () {
            this._selectStar(0);
          },
        ),
        IconButton(
          icon: this._showStar(1),
          iconSize: this._iconSize,
          color: this._iconsColor,
          onPressed: () {
            this._selectStar(1);
          },
        ),
        IconButton(
          icon: this._showStar(2),
          iconSize: this._iconSize,
          color: this._iconsColor,
          onPressed: () {
            this._selectStar(2);
          },
        ),
        IconButton(
          icon: this._showStar(3),
          iconSize: this._iconSize,
          color: this._iconsColor,
          onPressed: () {
            this._selectStar(3);
          },
        ),
        IconButton(
          icon: this._showStar(4),
          iconSize: this._iconSize,
          color: this._iconsColor,
          onPressed: () {
            this._selectStar(4);
          },
        ),
      ],);
  }

  Icon _showStar(final int index) {
    if (this._selected.elementAt(index) == true) {
      return this._fullStar;
    } else {
      return this._emptyStar;
    }
  }

  void _selectStar(final int index) async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('reviewValue', (index + 1).toString());

    this.setState(() {
      for(int i = 0; i < this._selected.length; i++) {
        if (i <= index) {
          this._selected[i] = true;
        } else {
          this._selected[i] = false;
        }
      }
    });
  }

}

class StarReview extends StatefulWidget {

  final Review _reviewDone;

  StarReview(this._reviewDone);

  @override
  State<StatefulWidget> createState() => _StarReview(this._reviewDone);
}
