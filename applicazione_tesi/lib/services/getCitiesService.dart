import 'dart:math';

import 'package:applicazione_tesi/models/city.dart';
import 'package:applicazione_tesi/models/freePlace.dart';
import 'package:applicazione_tesi/services/locationService.dart';
import '../utilities.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show json;

class GetCitiesService {

  Future<List<City>> attemptGetCities() async {
    final position = await LocationService().getCurrentPosition();
    var res = await http.get(Utilities.INTERNET_ADDRESS + 'cities/getCities/');
    if(res.statusCode == 200) {
      final List<City> cities = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        final lat = double.parse(json.decode(res.body)[i]['latitude']);
        final lon = double.parse(json.decode(res.body)[i]['longitude']);

        cities.add(City.fromJson(json.decode(res.body)[i],
                    this.getDistance(lat, lon, position.latitude, position.longitude))); // DA AGGIORNARE
      }
      cities.sort((a, b) => (a.distance - b.distance).floor());
      return cities;
    } else {
      return null;
    }
  }

  Future<List<FreePlace>> attemptGetFreePlaces(final int cityID) async {
    var res = await http.get(Utilities.INTERNET_ADDRESS + 'cities/getFreePlaces/' + cityID.toString());
    if(res.statusCode == 200) {
      final List<FreePlace> freePlaces = [];
      for(var i = 0; i < json.decode(res.body).length; i++) {
        freePlaces.add(FreePlace.fromJson(json.decode(res.body)[i]));
      }
      return freePlaces;
    } else {
      return null;
    }
  }

  double getDistance(final double cityLat, final double cityLon, final double latitude, final double longitude) {

    final radius = 6371;
    final degrees = 57.29578;

    final distance =  (radius * pi * sqrt(pow((latitude - cityLat), 2)
          + cos(latitude / degrees) * cos(cityLat / degrees) * pow(longitude - (cityLon), 2)) / 180);
    return distance;
  }
}