import 'package:connectivity/connectivity.dart';

class InternetService {

  static Future<bool> checkConnectivity() async {
    final result = await Connectivity().checkConnectivity();
    return result != ConnectivityResult.none;
  }
}