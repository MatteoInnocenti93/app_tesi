import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';

class LocationService {

  final Location _location = Location();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;


  Future<Position> getCurrentPosition() async {
    return this._checkPermissions().then((permissionsOK) {
      if (permissionsOK) {
        return Geolocator().getCurrentPosition();
      } else {
        return null;
      }
    });
  }

  Future<double> getDistanceFromPoint(final Position position) async {
    return this._checkPermissions().then((permissionsOK) {
      if (permissionsOK) {
        return Geolocator().distanceBetween(position.latitude, position.longitude, 44.005278, 12.665);
      } else {
        return null;
      }
    });
  }

  Future<String> getAddressFromPosition(final Position position) async {
    return this._checkPermissions().then((permissionsOK) {
      if (permissionsOK) {
        final Coordinates coordinates = Coordinates(position.latitude, position.longitude);
        return Geocoder.local.findAddressesFromCoordinates(coordinates).then((result) {
          return result.first.addressLine;
        });
      } else {
        return null;
      }
    });

  }

  Future<String> getCurrentAddress() async {
    return this._checkPermissions().then((permissionsOK) {
      if (permissionsOK) {
        return this.getCurrentPosition().then((position) {
          final Coordinates coordinates = Coordinates(position.latitude, position.longitude);
          final address = Geocoder.local.findAddressesFromCoordinates(coordinates).then((result) {
            return result.first.addressLine;
          });
          return address;
        });
      } else {
        return null;
      }
    });
  }

  Future<bool> _checkPermissions() async {
    _serviceEnabled = await this._location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await this._location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }
    _permissionGranted = await this._location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await this._location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }
}