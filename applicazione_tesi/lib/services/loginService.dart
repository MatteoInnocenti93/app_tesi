import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utilities.dart';
import 'package:http/http.dart' as http;

class LoginService {

  Future<String> attemptLogin(final String email, final String password) async {
    var res = await http.post(
        Utilities.INTERNET_ADDRESS + 'users/login',
        body: {
          "email": email,
          "password": password,
        }
    );
    if(res.statusCode == 200) {

      final String jwt =  res.body.split('#')[0];
      final String name =  res.body.split('#')[1];
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      FlutterSecureStorage().write(key: 'jwt', value: jwt);
      prefs.setString('email', email);
      prefs.setString('name', name);
      return res.body;
    } else {
      return null;
    }
  }
}