import 'package:applicazione_tesi/models/user.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../utilities.dart';
import 'dart:convert' show json;

class UserService {

  final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();

  Future<String> attemptContactServer(String email, String password, String name, String surname) async {
    final res =  await http.post(Utilities.INTERNET_ADDRESS + 'users/newUser', body: {
      'email'       : email,
      'password'    : password,
      'name'        : name,
      'surname'     : surname,
    });
    if (res.statusCode == 409) {
      return "Esiste già un account legato alla mail inserita.";
    }
    return "";
  }

  Future<User> attemptGetUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.get('email');
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final res =  await http.post(Utilities.INTERNET_ADDRESS + 'users/getUser', body: {
      'email'       : email,
      'jwt'         : jwt,
    });
    if (res.statusCode == 400) {
      return null;
    }
    print(res.body);
    return User.fromJson(json.decode(res.body));
  }

  Future<String> attemptUpdateUser(final String name, final String surname,
                                    final String oldPassword, final String password) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.get('email');
    final jwt = await FlutterSecureStorage().read(key: 'jwt');
    final res =  await http.post(Utilities.INTERNET_ADDRESS + 'users/updateUser', body: {
      'email'       : email,
      'jwt'         : jwt,
      'name'        : name,
      'surname'     : surname,
      'password'    : password,
      'oldpassword' : oldPassword
    });
    if (res.statusCode != 200) {
      return "Errore.";
    }
    return "";
  }
}