import 'dart:ui';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utilities {
  static const double FIELDS_WIDTH = 350;
  static const double FIELDS_HEIGHT = 37;
  static const double BUTTON_RADIUS = 30;
  static const double TEXT_LABELS_SIZE =  23;
  static const String INTERNET_ADDRESS = 'http://d7b630ca6304.ngrok.io/';
  static final double verticalRatio = window.physicalSize.height / 100;
  static final double horizontalRatio = window.physicalSize.width / 100;

  static void logout() async {
    FlutterSecureStorage().delete(key: "jwt");
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
  }
}