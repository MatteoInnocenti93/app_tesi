<?php namespace App\Controllers;

use App\Models\CityModel;
use App\Models\UserModel;
use App\Models\FavCityModel;
use App\Models\FreePlaceModel;

class Cities extends BaseController {

    private $cityModel;
    private $userModel;
    private $favCityModel;
    private $freePlaceModel;

    public function __construct() {
        $this->cityModel = new CityModel();
        $this->userModel = new UserModel();
        $this->favCityModel = new FavCityModel();
        $this->freePlaceModel = new FreePlaceModel();
    }

    //restituisce tutte le città
    public function getCities() {
       return json_encode($this->cityModel->findAll());
    }

    public function getFavCities() {
        if($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');

            $favCities = $this->favCityModel->query('SELECT * 
                                                     FROM fav_cities F JOIN cities C
                                                     ON F.city_name = C.name AND F.city_country = C.country
                                                     ')
                                            ->getResultArray(); //MIGLIORABILE
            
            $data = [];
            foreach($favCities as $city) {
                if ($city['email_user'] == $email) {
                    array_push($data, $city);
                }
            }

            if(count($data) == 0) {
                $this->response->setStatusCode(404); 
                return;
            }
            
            return json_encode($data);
        } else {
            echo "Non è post.";
        }
    }

    //aggiunge una città ai preferiti
    public function addFavCity() {

        if($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $city = $this->request->getPost('city');
            $country = $this->request->getPost('country');

            $user = $this->userModel->where(['email' => $email])->findAll();
            if(count($user) === 0) {
                $this->response->setStatusCode(404); 
                return;
            }

            $checkCity = $this->cityModel->where(['name' => $city, 'country' => $country])->findAll();
            if(count($checkCity) === 0) {
                $this->response->setStatusCode(404); 
                return;
            }

            $favCity = $this->favCityModel->where(['email_user' => $email, 
                                                'city_name' => $city, 
                                                'city_country' => $country])
                                        ->findAll();

            if (count($favCity) === 0) {

                $data = [
                    'email_user'    => $email,
                    'city_name'     => $city,
                    'city_country'  => $country
                ];

                if(!$this->favCityModel->save($data)) {
                    $this->response->setStatusCode(400); 
                    return;
                }
            } else {
                $this->favCityModel->delete($favCity[0]['id']);
            }
        } else {
            echo "Non è post.";
        }
    }

    public function getFreePlaces($cityID) {

        $freePlaces = $this->freePlaceModel->where(['city_id' => $cityID])->findAll();

        if (count($freePlaces) == 0) {
            $this->response->setStatusCode(404);
            return; 
        }
        return json_encode($freePlaces);
    }
}