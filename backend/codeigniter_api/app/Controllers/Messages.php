<?php namespace App\Controllers;

use App\Models\MessageModel;
use App\Models\RoomModel;

class Messages extends BaseController {

    private $messageModel;
    private $roomModel;

    public function __construct() {
        $this->messageModel = new MessageModel();
        $this->roomsModel = new RoomModel();
    }

    public function newMessage() {

        if ($this->request->getMethod() == 'post') {

            $emailReceiver = $this->request->getPost('receiver');
            $emailSender = $this->request->getPost('email_sender');
            $nameSender = $this->request->getPost('name_sender');
            $text = $this->request->getPost('text');

            $room = $this->roomsModel->where(['email_user' => $emailReceiver])->findAll();

            if (count($room) === 0) {
                $this->response->setStatusCode(404);
                return;
            } else {
                
                $data = [
                    'room_id'       => $room[0]['id'],
                    'email_sender'  => $emailSender,
                    'name_sender'   => $nameSender,
                    'text'          => $text
                ];

                if (!$this->messageModel->save($data)) {
                    $this->response->setStatusCode(400);
                    return;
                }
            }
        }
    }

    public function getMessagesInRoom() {
        if ($this->request->getMethod() == 'post') {

            $roomID = $this->request->getPost('room_id');

            $messages = $this->messageModel->where(['room_id' => $roomID])->findAll();

            if (count($messages) == 0) {
                $this->response->setStatusCode(404);
                return;
            }
            return json_encode($messages);
        }
    }

    public function deleteMessage() {
        if ($this->request->getMethod() == 'post') {

            $messageID = $this->request->getPost('message_id');

            if (!$this->messageModel->delete($messageID)) {
                $this->response->setStatusCode(400);
                return;
            }
        }
    }
    
    //it creates the jwt
    private function createJWT($email) {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Create token payload as a JSON string
        $payload = json_encode(['mail' => $email]);
        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'm yincredibl y(!!1!11!)<SECRET>)Key!', true);
        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }
}