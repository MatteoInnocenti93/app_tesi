<?php namespace App\Controllers;

use App\Models\ReviewModel;
use App\Models\RoomModel;

class Reviews extends BaseController {

    private $reviewModel;
    private $roomModel;

    public function __construct() {
        $this->reviewModel = new ReviewModel();
        $this->roomsModel = new RoomModel();
    }

    public function newReview() {

        if ($this->request->getMethod() == 'post') {

            $emailReceiver = $this->request->getPost('receiver');
            $emailSender = $this->request->getPost('email_sender');
            $nameSender = $this->request->getPost('name_sender');
            $text = $this->request->getPost('text');
            $value = $this->request->getPost('value');

            $room = $this->roomsModel->where(['email_user' => $emailReceiver])->findAll();

            if (count($room) === 0) {
                $this->response->setStatusCode(404);
                return;
            } else {

                $review = $this->reviewModel->where(['email_sender' => $emailSender, 
                                                     'room_id' => $room[0]['id']])                           
                                            ->findAll();
                
                $data = [
                    'room_id'       => $room[0]['id'],
                    'email_sender'  => $emailSender,
                    'name_sender'   => $nameSender,
                    'text'          => $text,
                    'value'         => $value,
                ];

                if (count($review) > 0) {
                    $data['id'] = $review[0]['id'];
                }

                if (!$this->reviewModel->save($data)) {
                    $this->response->setStatusCode(400);
                    return;
                }
            }
        }
    }

    public function getReviewsInRoom() {
        if ($this->request->getMethod() == 'post') {

            $roomID = $this->request->getPost('room_id');

            $reviews = $this->reviewModel->where(['room_id' => $roomID])->findAll();

            if (count($reviews) == 0) {
                $this->response->setStatusCode(404);
                return;
            }
            return json_encode($reviews);
        }
    }
    
    //it creates the jwt
    private function createJWT($email) {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Create token payload as a JSON string
        $payload = json_encode(['mail' => $email]);
        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'm yincredibl y(!!1!11!)<SECRET>)Key!', true);
        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }
}