<?php namespace App\Controllers;

use App\Models\RoomModel;
use App\Models\UserModel;
use App\Models\CityModel;

class Rooms extends BaseController {

    private $roomsModel;
    private $userModel;
    private $cityModel;

    public function __construct() {
        $this->roomsModel = new RoomModel();
        $this->userModel = new UserModel();
        $this->cityModel = new CityModel();
    }

    public function getClosestRooms($city, $country, $latitude, $longitude, $far) {

        $radius =     6371;
        $degrees =    57.29578;

        $rooms = $this->roomsModel->query('SELECT * 
                                           FROM users U, rooms R
                                           WHERE U.email = R.email_user')->getResultArray();
        if (count($rooms) === 0) {
            $this->response->setStatusCode(404); 
            return;
        }

        $closeRooms = [];
        foreach($rooms as $room) {
            if ($room['city'] == $city && $room['country'] == $country) {
                $distance = ($radius * pi() * sqrt(pow(($latitude - $room['latitude']), 2) + 
                            cos($latitude / $degrees) * cos($room['latitude'] / $degrees) * pow($longitude - ($room['longitude']), 2)) / 180); 
                if ($distance < $far) {
                    array_push($closeRooms, $room);
                }
            }
        }


        return json_encode($closeRooms);
    }   

    public function getRoomsInCity($city, $country) {

        $rooms = $this->roomsModel->query('SELECT * 
                                           FROM users U, rooms R
                                           WHERE U.email = R.email_user')->getResultArray();
        if (count($rooms) === 0) {
            $this->response->setStatusCode(404); 
            return;
        }

        $roomsToReturn = [];
        foreach($rooms as $room) {
            if ($room['city'] == $city && $room['country'] == $country) {
                array_push($roomsToReturn, $room);
            }
        }
        if (count($roomsToReturn) === 0) {
            $this->response->setStatusCode(404); 
            return;
        }

        return json_encode($roomsToReturn);
    }

    public function getRoom() {
        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $room = $this->roomsModel->where(['email_user' => $email])->findAll();
            $user = $this->userModel->where(['email' => $email])->findAll();

            if (count($room) === 0) {
                $this->response->setStatusCode(404); 
                return;
            } 

            $data = [
                'id'         => $room[0]['id'],
                'email_user' => $email,
                'name'       => $user[0]['name'],
                'surname'    => $user[0]['surname'],
                'contacts'   => $room[0]['contacts'],
                'work'       => $room[0]['work'],
                'interests'  => $room[0]['interests'],
                'tour'       => $room[0]['tour'],
                'latitude'   => $room[0]['latitude'],
                'longitude'  => $room[0]['longitude'],
                'city'       => $room[0]['city'],
                'country'    => $room[0]['country']
            ];
            
            
            return json_encode($data);
        } else {
            echo 'Non è post';
        }
    }

    public function newRoom() {
        if ($this->request->getMethod() == 'post') {

            $city = $this->cityModel->where(['name' => $this->request->getPost('city'), 
                                            'country' => $this->request->getPost('country')])
                                    ->findAll();

            if (count($city) == 0) {
                $this->response->setStatusCode(404);
                return;
            }

            $data = [
                'email_user' => $this->request->getPost('email'),
                'contacts'   => $this->request->getPost('contacts'),
                'work'       => $this->request->getPost('work'),
                'interests'  => $this->request->getPost('interests'),
                'tour'       => $this->request->getPost('tour'),
                'latitude'   => $this->request->getPost('latitude'),
                'longitude'  => $this->request->getPost('longitude'),
                'city'       => $this->request->getPost('city'),
                'country'    => $this->request->getPost('country'),
            ];

            if(!$this->roomsModel->save($data)) {
                $this->response->setStatusCode(400);
            }
        } else {
            echo 'Non è post';
        }
    }

    public function editRoom() {
        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $jwt = $this->request->getPost('jwt');

            if ($jwt != $this->createJWT($email)) {
                $this->response->setStatusCode(400);
                return;
            }

            $city = $this->cityModel->where(['name' => $this->request->getPost('city'), 
                                            'country' => $this->request->getPost('country')])
                                    ->findAll();

            if (count($city) == 0) {
                $this->response->setStatusCode(404);
                return;
            }

            $data = [
                'id'         => $this->request->getPost('room_id'),
                'email_user' => $this->request->getPost('email'),
                'contacts'   => $this->request->getPost('contacts'),
                'work'       => $this->request->getPost('work'),
                'interests'  => $this->request->getPost('interests'),
                'tour'       => $this->request->getPost('tour'),
                'latitude'   => $this->request->getPost('latitude'),
                'longitude'  => $this->request->getPost('longitude'),
                'city'       => $this->request->getPost('city'),
                'country'    => $this->request->getPost('country'),
            ];

            if(!$this->roomsModel->save($data)) {
                $this->response->setStatusCode(400);
            }
        } else {
            echo 'Non è post';
        }
    }

    public function deleteRoom() {
        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $jwt = $this->request->getPost('jwt');
            $idToDelete = $this->request->getPost('id_to_delete');

            if ($jwt != $this->createJWT($email)) {
                $this->response->setStatusCode(400);
                return;
            }

            if(!$this->roomsModel->delete($idToDelete)) {
                $this->response->setStatusCode(400);
            }
        } else {
            echo 'Non è post';
        }
    }

    //it creates the jwt
    private function createJWT($email) {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Create token payload as a JSON string
        $payload = json_encode(['mail' => $email]);
        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'm yincredibl y(!!1!11!)<SECRET>)Key!', true);
        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }
}