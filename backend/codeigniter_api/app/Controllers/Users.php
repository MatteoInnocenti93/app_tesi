<?php namespace App\Controllers;

use App\Models\UserModel;

class Users extends BaseController {

    private $userModel;

    public function __construct() {
        $this->userModel = new UserModel();
    }

    public function login() {

        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            $user = $this->userModel->where(['email' => $email, 'password' => $password])->findAll();

            if (count($user) === 0) {
                $this->response->setStatusCode(404);
            } else {
                if ($password != null) {
                    echo $this->createJWT($email).'#'.$user[0]['name'];
                } else {
                    echo json_encode($user);
                }
            }
        } else {
            echo 'Non è post';
        }
    }

    public function newUser() {
        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $data = [
                'email'    => $email,
                'password' => $this->request->getPost('password'),
                'name'     => $this->request->getPost('name'),
                'surname'  => $this->request->getPost('surname')
            ];

            $user = $this->userModel->where(['email' => $email])->findAll();

            if (count($user) === 0) {
                if(!$this->userModel->save($data)) {
                    $this->response->setStatusCode(400);
                }
            } else {
               $this->response->setStatusCode(409);
            }
        } else {
            echo 'Non è post';
        }
    }

    public function getUser() {
        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $jwt = $this->request->getPost('jwt');

            if ($jwt != $this->createJWT($email)) {
                $this->response->setStatusCode(400);
                return;
            }

            $user = $this->userModel->where(['email' => $email])->findAll();

            if (count($user) === 0) {
                $this->response->setStatusCode(404);
            } else {
               return json_encode($user[0]);
            }
        } else {
            echo 'Non è post';
        }
    }

    public function updateUser() {
        if ($this->request->getMethod() == 'post') {

            $email = $this->request->getPost('email');
            $jwt = $this->request->getPost('jwt');

            if ($jwt != $this->createJWT($email)) {
                $this->response->setStatusCode(400);
                return;
            }

            $user = $this->userModel->where(['email' => $email])->findAll();

            if (count($user) === 0) {
                $this->response->setStatusCode(404);
            } else {
               
                if ($this->request->getPost('password') == '') {
                    $data = [
                        'id'       => $user[0]['id'],
                        'name'     => $this->request->getPost('name'),
                        'surname'  => $this->request->getPost('surname')
                    ];
                } else {

                    $oldPassword = $this->request->getPost('oldpassword');
                    $user = $this->userModel->where(['email' => $email, 'password' => $oldPassword])->findAll();

                    if (count($user) == 0) {
                        $this->response->setStatusCode(404);
                        return;
                    }

                    $data = [
                        'id'       => $user[0]['id'],
                        'password' => $this->request->getPost('password'),
                        'name'     => $this->request->getPost('name'),
                        'surname'  => $this->request->getPost('surname')
                    ];
                }
  
                if(!$this->userModel->save($data)) {
                    $this->response->setStatusCode(400);
                }
            }
        } else {
            echo 'Non è post';
        }
    }

    //it creates the jwt
    private function createJWT($email) {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Create token payload as a JSON string
        $payload = json_encode(['mail' => $email]);
        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'm yincredibl y(!!1!11!)<SECRET>)Key!', true);
        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
    }
}