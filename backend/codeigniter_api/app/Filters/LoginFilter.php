<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\I18n\Time;

class LoginFilter implements FilterInterface
{
    public function before(RequestInterface $request)
    {
        if (session()->get()['email'] == null) {
            return redirect()->to('/pages/loginPage');
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response)
    {

    }
}