<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\I18n\Time;

class UserNotApprovedFilter implements FilterInterface
{
    public function before(RequestInterface $request)
    {
        $session = session()->get();
        if ($session['email'] != null && !$session['approved']) {
            return redirect()->to('/pages/userNotApprovedPage');
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response)
    {

    }
}