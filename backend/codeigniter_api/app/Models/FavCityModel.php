<?php namespace App\Models;

    use CodeIgniter\Model;

    class FavCityModel extends Model
    {
        protected $table = 'fav_cities';

        protected $primaryKey = 'id';

        protected $returnType = 'array';

        protected $allowedFields = ['email_user', 'city_name', 'city_country'];    
        
    }
?>