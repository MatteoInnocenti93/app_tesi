<?php namespace App\Models;

    use CodeIgniter\Model;

    class FreePlaceModel extends Model
    {
        protected $table = 'free_places';

        protected $primaryKey = 'id';

        protected $returnType = 'array';

        protected $allowedFields = ['city_id', 'start_x', 'start_y', 'end_x', 'end_y', 'offset'];    
        
    }
?>