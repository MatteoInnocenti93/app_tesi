<?php namespace App\Models;

    use CodeIgniter\Model;

    class MessageModel extends Model
    {
        protected $table = 'messages';

        protected $primaryKey = 'id';

        protected $returnType = 'array';

        protected $allowedFields = ['room_id', 'email_sender', 'name_sender', 'text'];  
        
        protected $useTimestamps = true;

        protected $createdField  = 'created_at'; 
        protected $updatedField  = 'updated_at';     
        
    }
?>