<?php namespace App\Models;

    use CodeIgniter\Model;

    class ReviewModel extends Model
    {
        protected $table = 'reviews';

        protected $primaryKey = 'id';

        protected $returnType = 'array';

        protected $allowedFields = ['room_id', 'email_sender', 'name_sender', 'text', 'value'];         
    }
?>