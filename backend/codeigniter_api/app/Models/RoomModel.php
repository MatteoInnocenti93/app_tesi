<?php namespace App\Models;

    use CodeIgniter\Model;

    class RoomModel extends Model
    {
        protected $table = 'rooms';

        protected $primaryKey = 'id';

        protected $returnType = 'array';

        protected $allowedFields = ['email_user', 'contacts', 'work', 'interests', 
                                    'tour', 'latitude', 'longitude', 'city', 'country', 'image_path'];    
        
    }
?>