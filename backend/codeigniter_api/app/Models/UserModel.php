<?php namespace App\Models;

    use CodeIgniter\Model;

    class UserModel extends Model
    {
        protected $table = 'users';

        protected $primaryKey = 'id';

        protected $returnType = 'array';

        protected $allowedFields = ['email', 'password', 'name', 'surname'];
        
        protected $validationRules    = [
            "email"                 => "required|max_length[255]",
            "password"              => "required|max_length[64]",
            "name"                  => "max_length[255]",
            "surname"               => "max_length[255]",
        ];      
    }
?>