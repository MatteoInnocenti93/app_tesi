-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `api_tesi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `api_tesi`;

DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `longitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `latitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `cities` (`id`, `name`, `country`, `province`, `longitude`, `latitude`) VALUES
(3,	'Cesena',	'Italy',	'FC',	'12.243',	'﻿44.133'),
(4,	'Funchal',	'Portugal',	'Madeira',	'-16.9',	'32.6333'),
(5,	'Milano',	'Italy',	'MI',	'9.1895100',	'45.4642700');

DROP TABLE IF EXISTS `fav_cities`;
CREATE TABLE `fav_cities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email_user` varchar(255) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `fav_cities` (`id`, `email_user`, `city_name`, `city_country`) VALUES
(1,	'inno@g.com',	'Cesena',	'Italy'),
(2,	'jack@g.com',	'Milano',	'Italy');

DROP TABLE IF EXISTS `free_places`;
CREATE TABLE `free_places` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city_id` int NOT NULL,
  `start_x` varchar(255) NOT NULL,
  `start_y` varchar(255) NOT NULL,
  `end_x` varchar(255) NOT NULL,
  `end_y` varchar(255) NOT NULL,
  `offset` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `free_places` (`id`, `city_id`, `start_x`, `start_y`, `end_x`, `end_y`, `offset`) VALUES
(1,	3,	'105',	'450',	'123',	'472',	'0'),
(2,	5,	'340',	'454',	'364',	'486',	'0'),
(3,	3,	'146',	'446',	'167',	'472',	'0'),
(4,	3,	'190',	'450',	'214',	'467',	'0'),
(5,	3,	'238',	'452',	'259',	'468',	'0'),
(6,	5,	'87',	'462',	'103',	'481',	'294'),
(7,	5,	'148',	'457',	'170',	'485',	'294'),
(8,	5,	'210',	'465',	'225',	'480',	'294');

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_id` int NOT NULL,
  `email_sender` varchar(255) NOT NULL,
  `name_sender` varchar(255) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `messages` (`id`, `room_id`, `email_sender`, `name_sender`, `text`, `created_at`, `updated_at`) VALUES
(1,	1,	'inno@g.com',	'Matteo	',	'Ciao a tutti!!',	'2020-09-17 20:13:03',	'2020-09-17 20:13:03'),
(2,	2,	'jack@g.com',	'Giacomo',	'Lasciate pure in messaggio in bacheca se volete informazioni!',	'2020-09-17 20:20:23',	'2020-09-17 20:20:23'),
(3,	1,	'jack@g.com',	'Giacomo',	'Ciao, piacere GiacomoInno!',	'2020-09-17 20:21:11',	'2020-09-17 20:21:11');

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_id` int NOT NULL,
  `email_sender` varchar(255) NOT NULL,
  `name_sender` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `reviews` (`id`, `room_id`, `email_sender`, `name_sender`, `text`, `value`) VALUES
(1,	1,	'inno@g.com',	'Matteo	',	'Stanza migliore di tutte!',	'5'),
(2,	2,	'jack@g.com',	'Giacomo',	'Booooh',	'3'),
(3,	1,	'jack@g.com',	'Giacomo',	'Noon male',	'4');

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email_user` varchar(255) NOT NULL,
  `contacts` varchar(255) NOT NULL,
  `work` varchar(255) NOT NULL,
  `interests` varchar(255) NOT NULL,
  `tour` bit(1) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `rooms` (`id`, `email_user`, `contacts`, `work`, `interests`, `tour`, `latitude`, `longitude`, `city`, `country`) VALUES
(1,	'inno@g.com',	'123456789',	'Prova lavoro',	'Prova interessi',	CONV('1', 2, 10) + 0,	'44.133',	'12.243',	'Cesena',	'Italy'),
(2,	'jack@g.com',	'Miei contatti',	'Mio lavoro',	'Miei interessi',	CONV('0', 2, 10) + 0,	'45.464 ',	'9.189',	'Milano',	'Italy');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `users` (`id`, `email`, `password`, `name`, `surname`) VALUES
(1,	'inno@g.com',	'Ciao11',	'Matteo	',	'Inno'),
(2,	'jack@g.com',	'Ciao11',	'Giacomo',	'Inno'),
(3,	'prova@gmail.com',	'Ciao11',	'Marco',	'Balissani');

-- 2020-09-17 18:27:27
