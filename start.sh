#!/bin/bash

cd backend/codeigniter_api

echo "building and starting backend--->"
docker-compose up --build -d

echo "starting ngrok --->"
cd ~/Documenti/ && ./ngrok http 8094
